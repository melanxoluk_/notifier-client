package com.melanxoluk.notifier.client

import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.util.concurrent.Executor


object Notifier {
    private val UTF8 = Charsets.UTF_8.name()
    var useSsl = false
    var host = "notifier.melanxoluk.com"
    var port = 80
    var path = "/notify"
    var key = ""

    /**
     * @throws IllegalStateException when api key is empty
     */
    @Throws(IllegalStateException::class)
    fun notify(msg: String, from: String? = null): Boolean {
        if (key.isEmpty()) {
            throw IllegalStateException("api key is empty")
        }

        val prefix = if (useSsl) "https://" else "http://"
        val port = if (port == 80) "" else ":$port"
        val path = "$prefix$host$port$path"
        return makeCall(path, msg, from)
    }

    private fun makeCall(path: String, msg: String, from: String?): Boolean {
        val fromArg = if (from.isNullOrEmpty()) "" else "&src=${URLEncoder.encode(from, UTF8)}"
        val url = "$path?msg=${URLEncoder.encode(msg, UTF8)}$fromArg"
        val connection = URL(url).openConnection() as HttpURLConnection
        connection.requestMethod = "GET"
        return connection.responseCode == HttpURLConnection.HTTP_OK
    }
}
