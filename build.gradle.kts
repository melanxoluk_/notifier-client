import com.jfrog.bintray.gradle.BintrayExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.gradle.api.tasks.bundling.Jar

plugins {
    `maven-publish`
    kotlin("jvm") version "1.2.71"
    id("com.jfrog.bintray") version "1.8.4"
}

group = "com.melanxoluk"
version = "0.1.3"
val notifierClient = "notifier-client"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib"))
}

val sourcesJar by tasks.creating(Jar::class) {
    classifier = "sources"
    // from(java.sourceSets["main"].allJava)
    from(kotlin.sourceSets["main"].kotlin)
}

val publicationName = notifierClient
publishing {
    publications.invoke {
        publicationName(MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar)
            afterEvaluate {
                artifactId = notifierClient
            }
        }
    }
}

bintray {
    user = System.getenv("BINTRAY_USER")
    key = System.getenv("BINTRAY_API")
    publish = true
    setPublications(publicationName)
    pkg(delegateClosureOf<BintrayExtension.PackageConfig> {
        repo = notifierClient
        name = notifierClient
        userOrg = "melanxoluk"
        vcsUrl = "https://gitlab.com/melanxoluk/notifier-client.git"
        websiteUrl = vcsUrl
        description = "Kotlin client example for simplifying make notifications"
        desc = description
        setLabels("kotlin")
        setLicenses("MIT")
    })
}

tasks {
    withType<KotlinCompile> {
        kotlinOptions.jvmTarget = "1.8"
    }

    withType<GenerateMavenPom> {
        destination = file("$buildDir/libs/$notifierClient.pom")
    }
}
